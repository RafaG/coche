// SALIDAS DIGITALES
const int MIA = 6;   // PD6
const int MIB = 5;   // PD5
const int MDA = 11;  // PB3
const int MDB = 3;   // PD3
const int LED = 1;   // PD1
const int LEDON = 7; // PD7

const int VELOCIDAD_MAXIMA = 100;
int velocidadIzquierda = 0;
int velocidadDerecha = 0;

int informacionIzquierda = -1;
int informacionDerecha = -1;
int informacionDirecciones = -1;
bool sincronizado = false;

const int maskDirIzquierda = 1;  // 0000 0001
const int maskDirDerecha = 2;    // 0000 0010

void setup(){
  pinMode(MIA, OUTPUT);
  pinMode(MIB, OUTPUT);
  pinMode(MDA, OUTPUT);
  pinMode(MDB, OUTPUT);
  pinMode(LED, OUTPUT);
  pinMode(LEDON, OUTPUT);
  

  digitalWrite(LEDON, HIGH); // Enciende las luces de los sensores
  
  Serial.begin(57600);
  
  delay(1000);
  digitalWrite(LED, LOW);    // Apaga el led rojo
  
}

int leeEntrada(){
  int b = -1;
  if (Serial.available() > 0) {
    b = Serial.read();
  }
  return b;
}

int comprobar(int velocidad){  
  //Comprobamos que la velocidad no supere la máxima.
  if (velocidad>VELOCIDAD_MAXIMA){
    velocidad = VELOCIDAD_MAXIMA;
  }
  
  return velocidad;
  
}

void setVelocidad(int izda, int dcha){
  setVelocidadIzquierda(izda);
  setVelocidadDerecha(dcha);
}

void setVelocidadIzquierda(int izda){
  // Motor izquierdo
  if (izda == 0) {
    digitalWrite(MDA, HIGH);
    digitalWrite(MDB, HIGH);
  } else if (izda > 0) {
    digitalWrite(MDA, HIGH);
    analogWrite(MDB, 255 - izda);
  } else {
    analogWrite(MDA, 255 + izda);
    digitalWrite(MDB, HIGH);
  }
}

void setVelocidadDerecha(int dcha){
   // Motor derecho
  if (dcha == 0) {
    digitalWrite(MIA, HIGH);
    digitalWrite(MIB, HIGH);
  } else if (dcha > 0) {
    digitalWrite(MIA, HIGH);
    analogWrite(MIB, 255 - dcha);
  } else {
    analogWrite(MIA, 255 + dcha);
    digitalWrite(MIB, HIGH);
  }

}

void loop(){
  
  int entrada = leeEntrada();
  //Si la entrada es 255 significa que los siguientes dos datos que recibiremos
  //son las velocidades de los motores izquierdo y derecho.
  if (entrada==255){
    sincronizado = true;
    //Se resetean las variables.
    informacionIzquierda = -1;
    informacionDerecha = -1;
    informacionDirecciones = -1;
    return; //!!!
  }
  
  if (sincronizado==true){
    if (informacionIzquierda==-1){
      //Como informacionIzquierda==-1, se sabe que lo que estamos recibiendo
      //es informacion referente al motor izquierdo.
      informacionIzquierda = entrada;
    }
    else if (informacionDerecha==-1){
      //Ahora sabemos que lo que recibimos es la velocidad del motor derecho.
      informacionDerecha = entrada;
    }
    else if (informacionDirecciones==-1){
      informacionDirecciones = entrada;
    }
    
  }
  
  //Se comprueba si ya tenemos toda la informacion necesaria (velocidades y direcciones).
  if (informacionIzquierda>=0 && informacionDerecha>=0 && informacionDirecciones>=0){
    
    int copiaIzquierda = velocidadIzquierda;
    int copiaDerecha = velocidadDerecha;
    
    //Se comprueba que las velocidades no sean mayores que lo permitido con el método
    //comprobar.
    velocidadIzquierda = comprobar(informacionIzquierda);
    //Y vemos la direccion.
    if ((informacionDirecciones&maskDirIzquierda) == 0){
      velocidadIzquierda = -velocidadIzquierda;
    }
    
    velocidadDerecha = comprobar(informacionDerecha);
    if ((informacionDirecciones&maskDirDerecha) == 0){
      velocidadDerecha = -velocidadDerecha;
    }
    
    //Comprobamos si se produce un cambio brusco de direccion para suavizarlo
    //Primero con el motor izquierdo.
    if ((copiaIzquierda>0 && velocidadIzquierda<0) || (copiaIzquierda<0 && velocidadIzquierda>0)){
      //Si es motor izquierdo se mueve hacia adelante y lo ponemos hacia atras,
      //primero lo ponemos a 0, o si se mueve hacia atras y lo ponemos hacia adelante.
      setVelocidadIzquierda(0);
    }
    
    if ((copiaDerecha>0 && velocidadDerecha<0) || (copiaDerecha<0 && velocidadDerecha>0)){
      setVelocidadDerecha(0);
    }
    
    //Se fija la velocidad.
    setVelocidad(velocidadIzquierda,velocidadDerecha);
    
    //Se pone sincronizado a false para prepararlo para la siguiente serie de mensajes.
    sincronizado = false;
    
  }

  
}